<?php
$var = 'The122.34343The';
$float_value_of_var = floatval($var);

echo $float_value_of_var; // 122.34343

$var = "";

echo "<br>";

if(empty($var)){
    echo '$var is empty';
}
else {
    echo '$var is not empty';
}
echo "<br>";

$var = array(1,2,3,4,5,6);


if(is_array($var)){
    echo '$var is an array';
}
else{
    echo '$var is not an array';
}
echo "<br>";

$serialize = serialize($var);
echo $serialize;
echo "<br>";

    $newvar = unserialize($serialize);
print_r($newvar);
echo "<br>";


var_dump($var);
echo "<br>";
echo "<br>";

var_export($var);
echo "<br>";
echo "<br>";

echo gettype($var);
echo "<br>";

unset($var);
print_r($var);

?>