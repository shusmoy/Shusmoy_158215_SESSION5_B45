<?php

    // bollean data type starts here
    $decision = true;
    if($decision){
        echo "the decision is True!<br>";
    }
    $decision = false;
    if($decision){
        echo "the desicion is False!";
    }
    //bollean end here

    //integer  and float Data types start here
    $value1 = 100; //ineteger
    $value2 = 55.35; //float
    //integer and float Data types end here

    //string example start here
    $myString1 = 'abcd1234#$% $value1';

    $myString2 = "abcd1234#$% $value1";
    echo $myString1 . "<br>";
    echo $myString2 . "<br>";

    $heredocStringVariable =<<<BITM
        heredoc line1 $value1 <br>
        heredoc line2 $value1 <br>
        heredoc line3 $value1 <br>
BITM;

echo $heredocStringVariable;


$nowdocString =<<<'BITM'
    nowdoc line1 $value1 <br>
    nowdoc line2 $value1 <br>
    nowdoc line3 $value1 <br>
BITM;

echo $nowdocString;

    //string example end here
?>